# Jave base image
FROM openjdk:8u332-jdk

# Expose ports
EXPOSE 8080 8000

# The application's jar file
ARG JAR_FILE=build/libs/simple-web-socket.jar

# Add the jar to the container
COPY ${JAR_FILE} simple-web-socket.jar

# Run the application
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000", "-jar", "/simple-web-socket.jar"]